const { check } = require("express-validator");

module.exports = {
  creation: [
    check("title", "Title is required.").notEmpty(),
    check("content", "Content is required.").notEmpty(),
    check("category", "Category is required.").notEmpty(),
  ],
  edition: [
    check("title", "Title is required.").notEmpty(),
    check("content", "Content is required.").notEmpty(),
    check("category", "Category is required.").notEmpty(),
  ],
};

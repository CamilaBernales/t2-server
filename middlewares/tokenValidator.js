const models = require('../models/index');
const jwt = require('jsonwebtoken');
const config = require('../config/config');

const validate = (req, res, next) => {
    const token = req.headers['authorization'].split(' ')[1];
    if(token){
        jwt.verify(token, config.secret, (err, decoded) => { 
            if(err) {
                res.status(403)
                res.set({
                    'Content-type': 'application/json'
                })
                res.send({error: 'The token is not valid.'})
            } else {
                req.decoded = decoded;
                next()
            }
        })
    } else {
        res.status(403)
        res.set({
            'Content-type': 'application/json'
        })
        res.send({error: 'Token is missing'})
    };   
};

module.exports = {
    validate
};

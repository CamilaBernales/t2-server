const { check } = require("express-validator");

const updateFields = [
    check('name').notEmpty().withMessage('Name is required.'),
    check('description').notEmpty().withMessage('Description is required.'),
    check('phone').notEmpty().withMessage('Phone is required.'),
    check('address').notEmpty().withMessage('Address is required.'),
    check('welcomeText').notEmpty().withMessage('Welcome text is required.'),
    check('facebookUrl').notEmpty().withMessage('The Facebook url is required.'),
    check('instagramUrl').notEmpty().withMessage('The Instagram url is required.'),
    check('linkedinUrl').notEmpty().withMessage('The Linkedin url is required.')
];

module.exports = {
    updateFields
};

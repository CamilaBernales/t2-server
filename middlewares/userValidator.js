const models = require('../models/index');
const { check } = require('express-validator');

const User = models.User;

const loginFields = [

    // Validate email
    check('email')
        .isEmail().withMessage('Must be an email')
        .custom( async value => {
            try {
                const user = await User.findOne({ where: {email: value}})
                if(user === null){
                    return Promise.reject('Email is not a valid')
                }else {
                    return true
                }
            } catch (error) {
                return Promise.reject('Can not connect with database.')
            }
        }),

    // Validate password
    check('password')
        .isLength({min: 6}).withMessage('Password must be at least 6 characters long')
]

const registerFields = [
    check('email', 'Email is not valid.').isEmail()
    .custom(async value => {
      try{
        let emailCheck = await User.findOne({ where:{ 'email': value }});
        if (emailCheck !== null) {
          return Promise.reject();
        }
      }catch (error) {
        return Promise.reject('No es posible validar el email'); 
      }
      }).withMessage('El email ya se encuentra registrado.'),
     check('firstName')
     .isLength({ min:1 }).withMessage('El nombre debe tener al menos dos caracteres')
     .isAlphanumeric().withMessage('Este campo debe ser alfanumérico'),
     check('lastName')
     .isLength({ min:1 }).withMessage('El apellido debe tener al menos dos caracteres')
     .isAlphanumeric().withMessage('Este campo debe ser alfanumérico'),
     check('password')
     .isLength({ min:6 }).withMessage('La clave debe tener al menos 6 caracteres')
]

const updateFields = [
    check('firstName')
        .isLength({ min:2 }).withMessage('Field must be higher that 1 character')
        .isAlphanumeric().withMessage('Field must be alphanumeric'),
    check('lastName')
        .isLength({ min:2 }).withMessage('Field must be higher that 1 character')
        .isAlphanumeric().withMessage('Field must be alphanumeric'),
]

module.exports = {
    loginFields,
    registerFields,
    updateFields
};
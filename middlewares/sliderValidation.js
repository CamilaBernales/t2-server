const { check } = require("express-validator");

module.exports = {
  creation: [
    // check("image", "Image is required.").notEmpty(),
    check("text", "Text is required.").notEmpty(),
    check("order", "Order is required.").notEmpty(),
  ],
};

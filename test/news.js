//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
chai.use(chaiHttp);
let should = chai.should();

//Our parent block
describe('News endpoint tests', () => {

    /*
  * Test the /GET route
  */
    describe('/GET news', () => {
        it('it should list all news', (done) => {
            chai.request(server)
                .get('/news')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object')
                    res.body.should.have.property('data')
                    res.body.data.should.be.a('array');
                    done();
                });
        });



    });


    describe('/POST news', () => {
        it('it no token sended on the headers, the news cant be created', (done) => {
            const newToCreate = {
                image: "https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png",
                type: "tipo de prueba",
                category: "news",
            }
            chai.request(server)
                .post('/news')
                .send(newToCreate)
                .end((err, res) => {
                    res.should.have.status(500);

                    done();
                });
        });

  

        it('it should create a new if all the fields are sended', (done) => {

            const newToCreate = {
                title: 'novedad de prueba',
                content: 'contenido de prueba',
                image:
                    "https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png",
                type: "tipo de prueba",
                category: "news",
            }
            chai.request(server)
                .post('/news')
                .send(newToCreate)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object')
                    res.body.should.have.property('message')
                    res.body.should.be.equal('Noticia creada correctamente')
                    done();
                });
        });



    });


});
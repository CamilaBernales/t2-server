const express = require('express');
const router = express.Router();
const userValidator = require('../middlewares/userValidator')
const controller = require('../controllers/authenticateUser');
const UserController = require('../controllers/UserController');

router.post('/login', userValidator.loginFields, controller.authenticateUserController);

router.post("/register", userValidator.registerFields ,UserController.create);

module.exports = router;
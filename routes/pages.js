const express = require("express");
const router = express.Router();
const pagesController = require("../controllers/pagesController");
const tokenValidator = require("../middlewares/tokenValidator");
const pageValidator = require('../middlewares/pageValidator');

router.get('/', tokenValidator.validate, pagesController.getAll);
router.patch('/:id', tokenValidator.validate, pageValidator.updateFields, pagesController.edit );
router.get('/page/:route', pagesController.getContent);

module.exports = router;
const express = require('express');
const app = express();

const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors')
require('dotenv').config()

/* Routers */

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const authenticateRouter = require("./routes/auth");
const organizationRouter = require("./routes/ong");
const newsRouter = require("./routes/news");
const sliderRouter = require("./routes/slider");
const createError = require('http-errors');
const pagesRouter = require('./routes/pages');
const awsTestRouter = require('./routes/awstest')


app.use(
  cors({
    exposedHeaders: "Authorization",
  })
);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger('dev'));
app.use(express.json({limit: '10000kb'}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/auth", authenticateRouter);
app.use("/organization/public", organizationRouter);
app.use("/news", newsRouter);
app.use("/slider", sliderRouter);
app.use('/pages', pagesRouter)
app.use('/filetest/', awsTestRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;

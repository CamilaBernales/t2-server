'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "pages",
      [
        {
          title: 'Cuarentena Solidaria',
          content: null,
          route: 'cuarentena-solidaria',
          organizationId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: 'Programa de líderes',
          content: null,
          route: 'programa-de-lideres',
          organizationId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: 'Teach me something I can’t Google',
          content: null,
          route: 'teach-me-something',
          organizationId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};

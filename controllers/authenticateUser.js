const bcrypt = require("bcrypt");
const models = require("../models/index");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const config = require("../config/config");

const User = models.User;

const authenticateUserController = async (req, res, next) => {
  const { email, password } = req.body;

  const errors = await validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({ errors: errors.array() });
  } else {
    try {
      const user = await User.findOne({ where: { email: email } });
      const result = bcrypt.compareSync(password, user.password);
      if (result) {
        const token = jwt.sign({ id: user.id }, config.secret);
        res.status(200);
        res.set({
          "Content-type": "application/json",
          Authorization: "bearer " + token,
        });
        res.send({
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          roleId: user.roleId,
        });
      } else {
        res.status(403);
        res.set({
          "Content-type": "application/json",
        });
        res.send({ error: "Password is not valid" });
      }
    } catch (error) {
      res.status(500);
      res.set({
        "Content-type": "application/json",
      });
      res.send({ error: "Can not connect with database" });
    }
  }
};

module.exports = {
  authenticateUserController,
};

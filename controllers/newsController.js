const models = require("../models");
const New = models.New;
const { validationResult } = require("express-validator");
const aws = require('../service/aws');

const create = async (req, res) => {
  const errors = validationResult(req);
  switch (req.file.mimetype) {
    case 'image/jpeg':
      break;
    case 'image/gif':
      break;
    case 'image/png':
      break;
    default:
      errors.errors.push({
        msg: 'The format is not valid.',
        param: 'image',
        location: 'body'
      })
      break;
  }
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  const { title, content, category } = req.body;
  try {
    const imageUrl = await aws.uploadFile(req.file.originalname, req.file.buffer)
    let post = await New.create({
      title: title,
      content: content,
      image: imageUrl.Location,
      category: category,
      type: "news",
    });
    await post.save();
    return res.status(200).json(post);
  } catch (error) {
    console.log(error);
    return res.status(505).json({ msg: "Sorry, an error ocurred." });
  }
};

const detail = async (req, res) => {
  try {
    const post = await New.findByPk(req.params.id);
    if (!post) {
      return res.status(404).json({ msg: "New not found." });
    }
    return res.status(200).json(post);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Sorry, an error ocurred." });
  }
};

const deleteNew = async (req, res) => {
  try {
    let post = await New.findByPk(req.params.id);
    if (!post) {
      return res
        .status(404)
        .json({ msg: "This new is not avalible or doesn't exists." });
    }
    await post.destroy();
    return res.status(200).json({ msg: "New deleted." });
  } catch (error) {
    return res.status(500).json({ msg: "Sorry, an error ocurred." });
  }
};

const allNews = async (req, res) => {
  try {
    const data = await New.findAll({attributes:[`id`, `title`, `createdAt`, 'content', 'image', 'category'], raw: true });
    if (!data) {
      return res.status(404).json({ msg: "Not exist news." });
    }
    return res.status(200).json({ data });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Sorry, an error ocurred." });
  }
};


const editNews = async (req, res) => {
  const errors = validationResult(req);
  if(req.file){
    switch (req.file.mimetype) {
      case 'image/jpeg':
        break;
      case 'image/gif':
        break;
      case 'image/png':
        break;
      default:
        errors.errors.push({
          msg: 'The format is not valid.',
          param: 'image',
          location: 'body'
        })
        break;
    }
  }
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  const { id, title, content, category } = req.body;
  console.log(req.body)
  console.log(req.file)
  try {
    let updateImg = ''
    if(req.file){
      const imageUrl = await aws.uploadFile(req.file.originalname, req.file.buffer)
      updateImg = await New.update({ image: imageUrl.Location }, {where: {id: id}})
    }
    const update = await New.update({
      title: title,
      content: content,
      category: category,
    }, { where: { id: id }});
    console.log(update[0])
    console.log(updateImg[0])
    if(update[0] === 0 && updateImg[0] === 0){
      res.status(403);
      res.set({
          'Content-type': 'application/json'
      })
      res.send({error: `The new's ID is not valid.`})
    }else {
      const newUpdated = await New.findOne({ where: {id: id}});
      res.status(200);
      res.set({
          'Content-type': 'application/json'
      })
      res.send(newUpdated)
    }
  } catch (error) {
    console.log(error);
    return res.status(505).json({ msg: "Sorry, an error ocurred." });
  }
};

const uploadImage = async (req, res)=>{
  console.log('llega', req.body)
  try {
    const imageUrl = await aws.uploadFile(req.file.originalname, req.file.buffer)
    res.status(200)
    res.set({
        'Content-type': 'application/json'
    })
    res.send({
      url: imageUrl.Location
    })
  } catch (error) {
    res.status(200)
    res.set({
        'Content-type': 'application/json'
    })
    res.send({
      error: 'The image could not be updloaded'
    })
  }
  
}


module.exports = { create, detail, deleteNew, allNews, editNews, uploadImage };


const models = require("../models");
const Slide = models.Slide;
const { validationResult } = require("express-validator");
const aws = require('../service/aws');


const create = async (req, res) => {
  const errors = validationResult(req);
  switch (req.file.mimetype) {
    case 'image/jpeg':
      break;
    case 'image/gif':
      break;
    case 'image/png':
      break;
    default:
      errors.errors.push({
        msg: 'The format is not valid.',
        param: 'image',
        location: 'body'
      })
      break;
  }
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  const { image, text, order, organizationID, deletedAt } = req.body;
  try {
    const imageUrl = await aws.uploadFile(req.file.originalname, req.file.buffer)
    let slide = await Slide.create({
      image: imageUrl.Location,
      text: text,
      order: order,
      organizationID: 1,
    });
    await slide.save();
    return res.status(200).json(slide);
  } catch (error) {
    console.log(error);
    return res.status(505).json({ msg: "Sorry, an error ocurred." });
  }
};

const slidersList = async (req, res) => {
  try {
    let sliders = await Slide.findAll({});
    if (!sliders) {
      return res.status(404).json({ msg: "No sliders." });
    }
    return res.status(200).json(sliders);
  } catch (error) {
    return res.status(505).json({ msg: "Sorry, an error ocurred." });
  }
};


const deleteSlider = async (req, res) => {
  try {
    let slide = await Slide.findByPk(req.params.id);
    if (!slide) {
      return res
        .status(404)
        .json({ msg: "This slide is not avalible or doesn't exists." });
    }
    await slide.destroy();
    return res.status(200).json({ msg: "Slide deleted." });
  } catch (error) {
    return res.status(500).json({ msg: "Sorry, an error ocurred." });
  }
};

module.exports = { create, slidersList, deleteSlider };

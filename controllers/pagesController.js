const { validationResult } = require('express-validator');
const models = require('../models/index');
const Pages = models.page;

const getAll = async (req, res, next) => {
    console.log('pide aca')
    try {
        const pages = await Pages.findAll({attributes:[`id`, `title`, `content`], raw: true })
        console.log('pages', pages)
        if(pages){
            res.status(200)
            res.set({
                'Content-type': 'application/json'
            })
            res.send(pages);
        } else {
            res.status(404)
            res.set({
                'Content-type': 'application/json'
            })
            res.send({ error: "There are no pages to show." });
        } 
    } catch (error) {
        console.log(error)
        res.status(404);
        res.set({
            'Content-type': 'application/json'
        })
        res.send({error: `Can't connect with database.`})
    }
};

const edit = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    try {
        const { id, content } = req.body;
        const page = await Pages.update( {content: content}, { where: {id: id}});
            if (page[0] === 0){
                res.status(403);
                res.set({
                    'Content-type': 'application/json'
                })
                res.send({error: `The page ID is not valid.`})
            } else {
                res.status(200)
                res.set({
                    'Content-type': 'application/json'
                })
                res.send({success: 'Page edited'})
            }
    } catch (error) {
        res.status(403);
        res.set({
            'Content-type': 'application/json'
        })
        res.send({error: `Can't connect with database.`})
    }
};

const getContent = async (req, res, next)=>{
    const {route} = req.params;
    try {
        const page = await Pages.findOne({ where: {route: route}});
        if (page === null){
            res.status(403);
            res.set({
                'Content-type': 'application/json'
            })
            res.send({error: `The route is not valid.`})
        } else {
            const content = {
                title: page.title,
                content: page.content,
            };
            res.status(200);
            res.set({
                'Content-type': 'application/json'
            })
            res.send(content)
        }
    } catch (error) {
        res.status(403);
        res.set({
            'Content-type': 'application/json'
        })
        res.send({error: `Can't connect with database.`})
    }
}

module.exports = {
    getAll,
    edit,
    getContent
}
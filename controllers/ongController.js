const { validationResult } = require("express-validator");
const models = require("../models");
const org = models.Organization;
const aws = require('../service/aws');


const findAll = (req, res) => {
  org
    .findByPk(1, {
      attributes: [
        `id`,
        `name`,
        `image`,
        `image`,
        'description',
        'welcomeText',
        `phone`,
        `address`,
        `facebookUrl`,
        `instagramUrl`,
        `linkedinUrl`,
      ],
    })
    .then((data) => {
      res.status(200).send(data);
    })
    .catch((error) => console.log(error));
};

const edit = async (req, res) => {
  const errors = validationResult(req);
  if(req.file){
    switch (req.file.mimetype) {
      case 'image/jpeg':
        break;
      case 'image/gif':
        break;
      case 'image/png':
        break;
      default:
        errors.errors.push({
          msg: 'The format is not valid.',
          param: 'image',
          location: 'body'
        })
        break;
    }
  }
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  const { id, name, description, phone, address, welcomeText, facebookUrl, instagramUrl, linkedinUrl } = req.body;
  try {
    let orgImage = '';
    if(req.file){
      const imageUrl = await aws.uploadFile(req.file.originalname, req.file.buffer)
      orgImage = await org.update({ image: imageUrl.Location }, { where: {id: id}})
    }
    const orgUpdated = await org.update({
      name: name, 
      description: description,
      phone: phone,
      address: address,
      welcomeText: welcomeText,
      facebookUrl: facebookUrl,
      instagramUrl: instagramUrl,
      linkedinUrl: linkedinUrl,
    }, { where: {id: id}});
    if (orgUpdated[0] === 0 && orgImage[0] === 0){
      res.status(403);
      res.set({
          'Content-type': 'application/json'
      })
      res.send({error: `The ID is not valid.`})
    } else {
      const ongUpdated = await org.findOne({where: {id: id}})
      res.status(200)
      res.set({
          'Content-type': 'application/json'
      })
      res.send(ongUpdated)
    }
  } catch (error) {
    console.log(error)
    res.status(403);
    res.set({
        'Content-type': 'application/json'
    })
    res.send({error: `Can't connect with database.`})
  }
};

module.exports = { findAll, edit };



